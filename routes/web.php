<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::get('/empleados', function () {
        return view('empleados.index');
    })->name('empleados');

    Route::get('/inicio', function () {
        return view('inicio.index');
    })->name('inicio');

    Route::get('/clientes', function () {
        return view('clientes.index');
    })->name('clientes');

    Route::get('/automovil', function () {
        return view('automovil.index');
    })->name('automovil');

    Route::get('/historial_autos', function () {
        return view('automovil.historial');
    })->name('historial_auto');

    Route::get('/reportes_autos', function () {
        return view('automovil.reportes');
    })->name('reportes_auto');


    Route::get('/pruebas', function () {
        return view('pruebas.index');
    })->name('pruebas');

    Route::get('/reportes', function () {
        return view('reportes.index');
    })->name('reportes');

    Route::get('/facturas', function () {
        return view('facturas.index');
    })->name('facturas');

    Route::get('/ingreso', function () {
        return view('clientes.ingreso_registro');
    })->name('ingreso');

});//middler AUTH