<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a href="{{route('inicio')}}"><i class="fa fa-home"></i> Inicio </a></li>
        <li><a href="{{route('clientes')}}" ><i class="fa fa-users"></i> Clientes</a></li>
        <li><a ><i class="fa fa-car"></i> automóvil <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
              <li><a href="{{route('automovil')}}">Listado de automóviles</a></li>
            <li><a href="{{route('historial_auto')}}">Historial de automóvil</a></li>
            <li><a href="{{route('reportes_auto')}}">Reporte de automóvil</a></li>
          </ul>
        </li>
        <li><a href="{{route('facturas')}}"><i class="fa fa-edit"></i> Facturas</a></li>

        <li><a ><i class="fa fa-wrench"></i> Pruebas <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="/pruebas#itecnico">Informe técnico</a></li>
            <li><a href="/pruebas#pcomercial">Peritraje comercial</a></li>
            <li><a href="/pruebas#pruta">Prueba de ruta</a></li>
            <li><a href="/pruebas#pvacio">Prueba de vacio</a></li>
            <li><a href="/pruebas#oinformes">Certificacion</a></li>
          </ul>
        </li>
        <li><a href="{{route('empleados')}}"><i class="fa fa-user"></i> Personal</a></li>
        <li><a><i class="fa fa-line-chart"></i> Reportes </a></li>
    </div>
  </div>
  <!-- /sidebar menu -->