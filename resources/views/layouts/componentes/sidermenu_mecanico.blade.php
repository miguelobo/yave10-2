<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a href="{{route('inicio')}}"><i class="fa fa-home"></i> Inicio </a>
         
        </li>
        <li><a><i class="fa fa-edit"></i> Pruebas <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="/pruebas#itecnico">Informe técnico</a></li>
            <li><a href="/pruebas#pcomercial">Peritraje comercial</a></li>
            <li><a href="/pruebas#pruta">Prueba de ruta</a></li>
            <li><a href="/pruebas#pvacio">Prueba de vacio</a></li>
            <li><a href="/pruebas#oinformes">Certificacion</a></li>
          </ul>
        </li>
      </ul>
    </div>

  </div>
  <!-- /sidebar menu -->