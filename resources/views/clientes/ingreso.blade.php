<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css"> 

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Switchery -->
 <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
 <link rel="stylesheet" href="css/jquery.dynameter.css">
<script src="js/jquery.dynameter.js"></script>
  <!-- PNotify -->
  <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
 

<link href="css/demo.css" rel="stylesheet" type="text/css" />
@include('clientes.ingreso.encabezado')
<div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Ingreso del cliente<small>Formulario de ingreso</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class=" form-label-left input_mask">
     
              @include('clientes.ingreso.informacion')
              @include('clientes.ingreso.informacion_vehiculo')


<div class="row center">
        <div class="ln_solid"></div>
        <h5 style="center">INVENTARIO DEL VEHICULO</h5>
        <div class="ln_solid"></div>
        </div>

      @include('clientes.ingreso.inventario')
        <!-- boton del estado del vehiculo -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Estado vehicular</button>
<div class="row col-md-12">

            <div class="col-md-12 col-sm-12 form-group center-block ">
                <div style="width: 546px; height: 396px;   background-image: url('images/dibujo1.png');" >
                <div class="center center-block" ><img id="imgestado" style=" width: 546px; height:  396px;" src="images/estado.png"  /></div>
                
                  </div>
          </div>
          <!-- Small modal -->
      <div class="row"></div>
      <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">OBSERVACIONES:</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <textarea class="resizable_textarea form-control" placeholder="Digite observaciones aqui..."></textarea>
            </div>
          </div>
          <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">PERTENENCIAS:</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea class="resizable_textarea form-control" placeholder="Digite pertenencias aqui..."></textarea>
                </div>
              </div>
              
  </div>

          <div class="row"></div>
          <div class="form-group">
                <div class="row"></div>
                <br>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".solicitud">Agregar</button>


              <div class="row"></div>
              <br>
               <table class="table table-striped table-bordered col-md-12 col-ms-12 col-xs-12">
                    <thead>
                            <tr>
                              <th>Solicitud del servicio</th>
                              <th>Trabajos realizados</th>
                            </tr>
                          </thead>
                
                
                          <tbody>
                            <tr>
                              <td>Cambio de bujiaaa</td>
                              <td>Se le realizo cambios de operaciones. de nmuchas mas cosas</td>
                            </tr>
                            <tr>
                                    <td>Solitud de cambio 1 </td>
                                    <td>Se le realizo cambios de operaciones. de nmuchas mas cosas</td>
                                  </tr>
                                  <td>Solitud de cambio 2 </td>
                                  <td>Se le realizo cambios de operaciones. de nmuchas mas cosas</td>
                                </tr>
                                <td>Solitud de cambio 3 </td>
                                <td>Se le realizo cambios de operaciones. de nmuchas mas cosas</td>
                              </tr>
                              <td>Solitud de cambio 4 </td>
                              <td>Se le realizo cambios de operaciones. de nmuchas mas cosas</td>
                            </tr>
                    </tbody>
               </table>
            </div>
  <!-- inicio modal -->          
 <div class="row"></div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Estado del automovil</h4>
      </div>
      <div class="modal-body">
        <h4>Dibuja el estado del automovil x si es un golpe - si es un rayon!</h4>
      @include('clientes.estadoAuto') 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="canvasManager.save();new PNotify({
          title: 'Estado actualizado!',
          text: 'Los datos del estado del automovil se han actualizado correctamente!!',
          type: 'success',
          styling: 'bootstrap3'
      });">Guardar</button>
      </div>

    </div>
  </div>
</div>
  <!-- fin modal -->   
    <!-- inicio modal -->          
 <div class="row"></div>
 <div class="modal fade solicitud" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
     <div class="modal-content">
 
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
         </button>
         <h4 class="modal-title" id="myModalLabel">Solicitud de servicio</h4>
       </div>
       <div class="modal-body">
         <h4>Informacion Solicitud!</h4>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
         <button type="button" class="btn btn-primary" onclick="canvasManager.save();new PNotify({
           title: 'Estado actualizado!',
           text: 'Los datos del estado del automovil se han actualizado correctamente!!',
           type: 'success',
           styling: 'bootstrap3'
       });">Guardar</button>
       </div>
 
     </div>
   </div>
 </div>
   <!-- fin modal -->   

            <div class="row"></div>
                <div class="form-group btn-group btn-group-sm">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary">Cancelar</button>
                            <button class="btn btn-primary" type="reset">Resetear</button>
                            <button type="submit" class="btn btn-success" onclick="new PNotify({
                              title: 'Ingreso exitoso!',
                              text: 'Los datos de ingreso se han guardado exitosamente!!',
                              type: 'success',
                              styling: 'bootstrap3'
                          });">Registrar</button>
                        </div>
                </div>
    
              </form>
            </div>
          </div>
        </div>
    </div>
      <!-- Switchery -->
      <script src="../vendors/switchery/dist/switchery.min.js"></script>
      <!-- Autosize -->
          <script src="../vendors/autosize/dist/autosize.min.js"></script>
      <!-- jQuery autocomplete -->
          <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
              <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>

<script src="">    
         var $myFuelGauge;

$( function () {
  $myFuelGauge = $("div#fuel-gauge").dynameter({
    width: 200,
    label: 'fuel',
    value: 7.5,
    min: 0.0,
    max: 15.0,
    unit: 'gal',
    regions: { // Value-keys and color-refs
      0: 'error',
      .5: 'warn',
      1.5: 'normal'
    }
  });

  // jQuery UI slider widget
  $('div#fuel-gauge-control').slider({
    min: 0.0,
    max: 15.0,
    value: 7.5,
    step: .1,
    slide: function (evt, ui) {
      $myFuelGauge.changeValue((ui.value).toFixed(1));
    }
  });

});
</script>

<script src="js/canvasPaint.js" type="text/javascript"></script>

<script type="text/javascript">
    var canvasManager;
        canvasManager = $(".canvas").canvasPaint();
        canvasManager.changeOpacity(50)
        canvasManager.loadBackgroundImage('images/dibujo1.png')
  
</script>