@extends('layouts.app')
@section('content')
<div class="title_left">
    <h3>Listado de automóviles</h3>
</div>
@include('automovil.tabla')
@include('automovil.formulario')
@endsection