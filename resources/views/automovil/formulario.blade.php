<div class="row">
        <div class="col-md-4 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Formulario<small>Autos</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-left input_mask">
            
                    <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Placa">
                            <span class="fa fa-pencil form-control-feedback left" aria-hidden="true"></span>
                   </div>

                 <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Marca">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>


                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Clase">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                 <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Moodelo">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>
                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Servicio">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>
                
                

                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Cilindraje">
                        <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Combustible">
                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>
            

                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" class="form-control has-feedback-left" id="inputSuccess3" placeholder="Kilometraje">
                  <span class="fa fa-keyboard-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-sm-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="inputSuccess3" placeholder="Tip caja">
                        <span class="fa fa-keyboard-o form-control-feedback left" aria-hidden="true"></span>
                 </div>
    
               
                <div class="ln_solid"></div>
                <div class="form-group btn-group btn-group-sm">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary">Cancelar</button>
                            <button class="btn btn-primary" type="reset">Resetear</button>
                            <button type="submit" class="btn btn-success">Registrar</button>
                        </div>
                </div>
    
              </form>
            </div>
          </div>
        </div>
    </div>