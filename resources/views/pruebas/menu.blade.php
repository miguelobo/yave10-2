<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
                <h2><i class="fa fa-bars"></i> Pruebas <small>Automóvil</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="col-xs-3">
              <!-- required for floating -->
              <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#home" data-toggle="tab">Procesos y/o reparaciones</a>
                </li>
                <li><a href="#pruta" data-toggle="tab">Prueba de ruta</a>
                </li>
                <li><a href="#pvacio" data-toggle="tab">Prueba de vacio</a>
                </li>
                <li><a href="#itecnico" data-toggle="tab">Informe técnico</a>
                </li>
                <li><a href="#pcomercial" data-toggle="tab">Perimetraje comercial</a>
                </li>
                <li><a href="#oinformes" data-toggle="tab">Otros Informes</a>
                </li>
              </ul>
            </div>

                        <div class="col-xs-9">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                            <p class="lead">Procesos y/o reparaciones</p>
                                            @include('pruebas.pruebas.reparaciones')
                                            </div>
                                            <div class="tab-pane" id="pruta">
                                                    @include('pruebas.pruebas.pruebaruta')
                                            </div>
                                            <div class="tab-pane" id="pvacio">
                                                @include('pruebas.pruebas.pruebaVacio')
                                            </div>
                                            <div class="tab-pane" id="itecnico">
                                                    @include('pruebas.pruebas.informetecnico')
                                            </div>
                                            <div class="tab-pane" id="pcomercial">
                                                    @include('pruebas.pruebas.pruebaPeritaje')
                                            </div>
                                            <div class="tab-pane" id="oinformes">Otros informes.</div>
                                </div>
                        </div>

            <div class="clearfix"></div>

          </div>
        </div>
      </div>