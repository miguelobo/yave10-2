
<div class="input-group">
        <input type="text" class="form-control" placeholder="Placa ó Cedula " aria-label="Text input with dropdown button">
        <div class="input-group-btn">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Busqueda por: <span class="caret"></span>
          </button>
          <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a href="#">Placa</a>
            </li>
            <li><a href="#">Cedula</a>
            </li>
          </ul>
        </div>
        <!-- /btn-group -->
      </div>

   <!--///////////////////////////////////////// -->
<div class="x_panel">
        <div class="x_title">
          <h2>Datos <small>Automóvil</small></h2>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-label-left input_mask">

       <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                      <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-6">Placa</label>
                          <div class="col-md-10 col-sm-10 col-xs-6">
                            <input type="text" class="form-control" disabled="disabled" placeholder="Placa">
                          </div>
                  </div>
                    <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-6">Modelo</label>
                            <div class="col-md-10 col-sm-10 col-xs-6">
                              <input type="text" class="form-control" disabled="disabled" placeholder="Modelo">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-6">Marca</label>
                            <div class="col-md-10 col-sm-10 col-xs-6">
                              <input type="text" class="form-control" disabled="disabled" placeholder="Marca">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-6">Cilindraje</label>
                            <div class="col-md-10 col-sm-10 col-xs-6">
                              <input type="text" class="form-control" disabled="disabled" placeholder="Cilindraje">
                            </div>
                    </div>
           </div>
            
                
           <div class="col-md-6 col-sm-12 col-xs-12 form-group">
             <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-6">Propietario</label>
                    <div class="col-md-10 col-sm-10 col-xs-6">
                      <input type="text" class="form-control" disabled="disabled" placeholder="Propietario">
                    </div>
            </div>
            <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-6">Cedula</label>
                    <div class="col-md-10 col-sm-10 col-xs-6">
                      <input type="text" class="form-control" disabled="disabled" placeholder="Cedula">
                    </div>
            </div>
            <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-6">Dirección</label>
                    <div class="col-md-10 col-sm-10 col-xs-6">
                      <input type="text" class="form-control" disabled="disabled" placeholder="Dirección">
                    </div>
            </div>
           </div>
                
           
            
            
            

          </form>
        </div>
        @include('pruebas.tabla2')
      </div>
   <!--///////////////////////////////////////// -->