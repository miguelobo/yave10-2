
<div class="clearfix"></div>
<section id="cualquiercosa" class="content invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12 invoice-header">
      <h1>
                      <i ><img src="images/logo.jpg" width="150px;" > </i> Peritaje comercial.<div class="pull-right"><img src="images/logo3.jpg" width="150px;" ></div></h1>
                    <h5> <small class="pull-right">Fecha: 15/02/2019   </small></H5> 
                  
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-3 invoice-col">
      De:
      <address>
                      <strong>YAVE 10, Servicios automotriz.</strong>
                      <br>Valledupar, calle 16-123
                      <br>barrio, Mayales
                      <br>Telefono: +(57) 3003354893
                      <br>Email: yave10@hotmail.com
                  </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-3 invoice-col">
      A:
      <address>
                      <strong>Miguel Lobo</strong>
                      <br>Cc:  1065832412
                      <br>Valledupar, Cesar
                      <br>Telefono: +57 30033354893
                      <br>Email: migueldavidlm@gmail.com
                  </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-3 invoice-col">
      Factura N° #007612
      <br>
      <b>Empleado:</b> Juanito Perez
      <br>
      <b>Cc:</b> 107678892
      <br>
      <b>Fecha:</b> 17/02/2019
      <br>
      <b>codigo:</b> ASDK123
    </div>
    <!-- /.col -->
     <!-- /.col -->
     <div class="col-sm-3 invoice-col">
            Automóvil
            <br>
            <b>Marca:</b> NISSAN
            <br>
            <b>Placa:</b> ASDK123
            <br>
            <b>Modelo:</b> 2000
            <br>
            <b>Tipo:</b> Particular
          </div>
          <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Cantidad</th>
            <th>Producto ó servicio</th>
            <th>codigo</th>
            <th style="width: 59%">Descripción</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Reparacion 1</td>
            <td>455-981-221</td>
            <td>El snort testosterone trophy driving gloves handsome gerry Richardson helvetica tousled street art master testosterone trophy driving gloves handsome gerry Richardson
            </td>
            <td>$64.50</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Prueba de ruta</td>
            <td>247-925-726</td>
            <td>Wes Anderson umami biodiesel</td>
            <td>$50.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Prueba de vacio</td>
            <td>735-845-642</td>
            <td>Terry Richardson helvetica tousled street art master, El snort testosterone trophy driving gloves handsome letterpress erry Richardson helvetica tousled</td>
            <td>$10.70</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Informe técnico</td>
            <td>422-568-642</td>
            <td>Tousled lomo letterpress erry Richardson helvetica tousled street art master helvetica tousled street art master, El snort testosterone</td>
            <td>$25.99</td>
          </tr>
          <tr>
                <td>1</td>
                <td>Perimetraje comercial</td>
                <td>422-568-642</td>
                <td>Tousled lomo letterpress erry Richardson helvetica tousled street art master helvetica tousled street art master, El snort testosterone</td>
                <td>$25.99</td>
              </tr>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
      <p class="lead">Metodos de pago:</p>
      <img src="images/visa.png" alt="Visa">
      <img src="images/mastercard.png" alt="Mastercard">
      <img src="images/american-express.png" alt="American Express">
      <img src="images/paypal.png" alt="Paypal">
      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        Este documento esta amparado por las normas nacionales e internacionales que protegen los derechos de Autor. Prohibido su ejecución o reproducción Total o Parcial como su traducción a cualquier idioma sin autorización escrita de su titular.
      </p>
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <p class="lead">Amount Due 2/22/2014</p>
      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>$250.30</td>
            </tr>
            <tr>
              <th>Tax (9.3%)</th>
              <td>$10.34</td>
            </tr>
            <tr>
              <th>Shipping:</th>
              <td>$5.80</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>$265.24</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  
</section>
 <!-- this row will not appear when printing -->
 <div class="row no-print">
        <div class="col-xs-12">
          <button id="btn" class="btn btn-default" ><i class="fa fa-print"></i> Imprimir</button>
          <button  class="btn btn-success pull-right" ><i class="fa fa-credit-card"></i> Pagar</button>
          <button   class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generar PDF</button>
        </div>
      </div>
<!--   final this row will not appear when printing -->
