@extends('layouts.app')
@section('content')
<div class="title_left">
        <h3>Facturas</h3>
    </div>

    <div class="x_panel">
            <div class="x_title">
                <h2>Facturas <small>Reportes</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
                @include('facturas.tabla2')
            </div>
    </div>
    
@include('facturas.factura')

 
 
    
    <script>
        $(document).on("click", "#btn", function(e) {
            $('#cualquiercosa').printElement({printMode:'popup',pageTitle:'Sistema de facturacion yave 10'}); 
        });
    </script>
@endsection